

-- TSRESNEW insert data into temp table
INSERT INTO ##HRIS_EmployeeExtract (
	StaffID, OracleID, StaffName, FirstName, LastName, EmployeeStatus, EmploymentType, 
	JoiningDate, dateofleaving, emailaddress, LegalEmployer, CostCentre, CostCentreType, 
	ActiveDirectoryAccount, LineManager, JobPosition, JobName, JobFamily, JobCode, jobfunction, 
	OrganizationalUnit, GradeCode, GradeName, PackageType, Location, LocationCode, 
	LocationCountry, CountryOfWorkCode, Department, SourceSystem, SwitchOverWave, client
	)
SELECT *, CASE WHEN locationcountry = 'Philippines' THEN 'PH' WHEN locationcountry = 'Indonesia' THEN 
				'ID' WHEN countryofworkcode COLLATE SQL_Latin1_General_CP1_CI_AS IN (
				SELECT rel_value
				FROM ##templivestaff
				) THEN 'SX' ELSE 'SC' END AS client
FROM [GB3ST4-BI01.SCI.STC.LOCAL].[SCI_RD_FMS].[Agresso].[Employees]
--test murima
SELECT rel.resource_id, rel.client, rel.date_to AS date_to, res.STATUS
INTO ##LiveCountries
FROM ahsrelvalue rel
INNER JOIN ##HRIS_EmployeeExtract hr ON hr.StaffID = rel.resource_id AND rel.client = hr.client
INNER JOIN ahsresources res ON res.resource_id = hr.StaffID AND res.client = hr.client AND rel.client = 
	res.client
WHERE rel_attr_id = 'C9' AND rel_value IN (
		SELECT att_value
		FROM aglrelvalue
		WHERE rel_attr_id = '2106' AND attribute_id = 'C9' AND rel_value != '0'
		)

